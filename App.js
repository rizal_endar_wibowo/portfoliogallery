import React from 'react';
import AppRouter from './src/router';
import firebase from '@react-native-firebase/app';
import OneSignal from 'react-native-onesignal';
import codepush from 'react-native-code-push';
import {Alert} from 'react-native';

var firebaseConfig = {
  apiKey: 'AIzaSyBgbkFxogG7vij0rSbCkriSQk3MB9oAoFQ',
  authDomain: 'portfoliogallery-836ca.firebaseapp.com',
  databaseURL: 'https://portfoliogallery-836ca.firebaseio.com',
  projectId: 'portfoliogallery-836ca',
  storageBucket: 'portfoliogallery-836ca.appspot.com',
  messagingSenderId: '349938612476',
  appId: '1:349938612476:web:cfa26ac8e051f02db97f79',
  measurementId: 'G-6GYCGBSXLC',
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App = () => {
  React.useEffect(() => {
    OneSignal.setLogLevel(6, 0);

    OneSignal.init('710c61c7-0dbe-42b1-9567-89d342419541', {
      kOSSettingsKeyAutoPrompt: false,
      kOSSettingsKeyInAppLaunchURL: false,
      kOSSettingsKeyInFocusDisplayOption: 2,
    });
    OneSignal.inFocusDisplaying(2);

    OneSignal.addEventListener('received', OnReceived);
    OneSignal.addEventListener('opened', OnOpened);
    OneSignal.addEventListener('ids', OnIds);

    codepush.sync(
      {
        updateDialog: true,
        installMode: codepush.InstallMode.IMMEDIATE,
      },
      SyncStatus,
    );

    return () => {
      OneSignal.removeEventListener('received', OnReceived);
      OneSignal.removeEventListener('opened', OnOpened);
      OneSignal.removeEventListener('ids', OnIds);
    };
  }, []);

  const SyncStatus = (status) => {
    switch (status) {
      case codepush.SyncStatus.CHECKING_FOR_UPDATE:
        console.log('checking for update');
        break;
      case codepush.SyncStatus.DOWNLOADING_PACKAGE:
        console.log('downloading package');
        break;
      case codepush.SyncStatus.UP_TO_DATE:
        console.log('Uptodate');
        break;
      case codepush.SyncStatus.INSTALLING_UPDATE:
        console.log('installing update');
        break;
      case codepush.SyncStatus.UPDATE_INSTALLED:
        Alert.alert('Notification', 'Update Installed');
        break;
      case codepush.SyncStatus.AWAITING_USER_ACTION:
        console.log('await user action');
      default:
        break;
    }
  };

  const OnReceived = (notification) => {
    console.log('onreceived', notification);
  };

  const OnOpened = (openResult) => {
    console.log('onopen', openResult);
  };

  const OnIds = (device) => {
    console.log('onids', device);
  };

  return <AppRouter />;
};

export default App;
