import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, processColor} from 'react-native';
import {LineChart, BarChart} from 'react-native-charts-wrapper';

const Chart = () => {
  return (
    <View style={styles.container}>
      <LineChart
        style={styles.chart}
        data={{
          dataSets: [
            {
              values: [
                {
                  y: 65,
                  x: 0,
                  marker: '65',
                },
                {
                  y: 77,
                  x: 1,
                  marker: '77',
                },
                {
                  y: 76,
                  x: 2,
                  marker: '76',
                },
                {
                  y: 74,
                  x: 3,
                  marker: '74',
                },
                {
                  y: 86,
                  x: 4,
                  marker: '86',
                },
                {
                  y: 95,
                  x: 5,
                  marker: '95',
                },
                {
                  y: 35,
                  x: 6,
                  marker: '35',
                },
                {
                  y: 47,
                  x: 7,
                  marker: '47',
                },
                {
                  y: 46,
                  x: 8,
                  marker: '46',
                },
                {
                  y: 44,
                  x: 9,
                  marker: '44',
                },
                {
                  y: 46,
                  x: 10,
                  marker: '46',
                },
                {
                  y: 35,
                  x: 11,
                  marker: '35',
                },
              ],
              label: '',
              config: {
                mode: 'CUBIC_BEZIER',
                drawValues: false,
                lineWidth: 2,
                drawCircles: true,
                circleColor: processColor('#004A7C'),
                drawCircleHole: false,
                circleRadius: 5,
                highlightColor: processColor('transparent'),
                color: processColor('#004A7C'),
                drawFilled: true,
                fillGradient: {
                  colors: [processColor('#004A7C'), processColor('#BED8FB')],
                  positions: [0, 0.5],
                  angle: 90,
                  orientation: 'TOP_BOTTOM',
                },
                fillAlpha: 1000,
                valueTextSize: 15,
              },
            },
          ],
        }}
        chartDescription={{text: ''}}
        legend={{
          enabled: false,
        }}
        marker={{
          enabled: true,
          markerColor: processColor('white'),
          textColor: processColor('black'),
        }}
        xAxis={{
          enabled: true,
          granularity: 1,
          drawLabels: true,
          position: 'BOTTOM',
          drawAxisLine: true,
          drawGridLines: false,
          fontFamily: 'HelveticaNeue-Medium',
          fontWeight: 'bold',
          textSize: 12,
          textColor: processColor('gray'),
          valueFormatter: [
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Agu',
            'Sep',
            'Oct',
            'Nov',
            'Des',
          ],
        }}
        yAxis={{
          left: {
            enabled: false,
          },
          right: {
            enabled: false,
          },
        }}
        autoScaleMinMaxEnabled={true}
        animation={{
          durationX: 0,
          durationY: 1500,
          easingY: 'EaseInOutQuart',
        }}
        drawGridBackground={false}
        drawBorders={false}
        touchEnabled={true}
        dragEnabled={false}
        scaleEnabled={false}
        scaleXEnabled={false}
        scaleYEnabled={false}
        pinchZoom={false}
        doubleTapToZoomEnabled={false}
        dragDecelerationEnabled={true}
        dragDecelerationFrictionCoef={0.99}
        keepPositionOnRotation={false}
        onChange={(event) => console.log(event.nativeEvent)}
      />
    </View>
  );
};

export default Chart;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  chart: {
    height: 100,
  },
});
