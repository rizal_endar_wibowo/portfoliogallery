import React from 'react';
import {
  StyleSheet,
  Text,
  Dimensions,
  TouchableOpacity,
  Image,
} from 'react-native';

const Menu = ({active, source, label, onPress}) => {
  const styles = StyleSheet.create({
    menu: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      width: Dimensions.get('window').width,
      paddingHorizontal: 5,
      marginTop: -30,
    },
    menuItem: {
      flex: 1,
      marginHorizontal: 5,
      borderRadius: 10,
      padding: 10,
      backgroundColor: active ? '#BED8FB' : 'white',
      elevation: 2,
      alignItems: 'center',
    },

    imageItem: {
      width: 50,
      height: 50,
    },

    labelItem: {
      fontSize: 12,
      color: active ? 'white' : '#005691',
      marginTop: 5,
    },
  });
  return (
    <TouchableOpacity style={[styles.menuItem, styles.a]} onPress={onPress}>
      <Image source={source} style={styles.imageItem} />
      <Text style={styles.labelItem}>{label}</Text>
    </TouchableOpacity>
    //   <TouchableOpacity style={styles.menuItem}>
    //     <Image
    //       source={require('../../assets/images/website.png')}
    //       style={styles.imageItem}
    //     />
    //     <Text style={styles.labelItem}>Website</Text>
    //   </TouchableOpacity>
    //   <TouchableOpacity style={styles.menuItem}>
    //     <Image
    //       source={require('../../assets/images/uiux.png')}
    //       style={styles.imageItem}
    //     />
    //     <Text style={styles.labelItem}>UI/UX</Text>
    //   </TouchableOpacity>
    // </View>
  );
};

export default Menu;
