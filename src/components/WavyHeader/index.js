import React from 'react';
import {View} from 'react-native';
import Svg, {Path} from 'react-native-svg';

const WavyHeader = ({customStyles}) => {
  return (
    <View style={customStyles}>
      <View style={{backgroundColor: '#005691', height: 160}}>
        <Svg
          height="60%"
          width="100%"
          viewBox="0 0 1440 320"
          style={{position: 'absolute', top: 130}}>
          <Path
            fill="#005691"
            d="M0,288L40,288C80,288,160,288,240,266.7C320,245,400,203,480,197.3C560,192,640,224,720,245.3C800,267,880,277,960,272C1040,267,1120,245,1200,229.3C1280,213,1360,203,1400,197.3L1440,192L1440,0L1400,0C1360,0,1280,0,1200,0C1120,0,1040,0,960,0C880,0,800,0,720,0C640,0,560,0,480,0C400,0,320,0,240,0C160,0,80,0,40,0L0,0Z"
          />
        </Svg>
      </View>
    </View>
  );
};

export default WavyHeader;
