import React from 'react';
import Intro from '../screens/Intro';
import Login from '../screens/Login';
import Splash from '../screens/Splash';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Register from '../screens/Register';
import Home from '../screens/Home';
import Profile from '../screens/Profile';
import Icon from 'react-native-vector-icons/Ionicons';
import Chat from '../screens/Chat';
import {AuthContext} from '../context';
import AsyncStorage from '@react-native-community/async-storage';

const Stack = createStackNavigator();

const AuthScreen = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
  <Tabs.Navigator
    initialRouteName="Home"
    tabBarOptions={{
      activeTintColor: '#005691',
    }}>
    <Tabs.Screen
      name="Home"
      component={Home}
      options={{
        tabBarLabel: 'Home',
        tabBarIcon: ({color, focused}) => (
          <Icon
            name={focused ? 'home' : 'home-outline'}
            color={color}
            size={26}
          />
        ),
      }}
    />
    <Tabs.Screen
      name="Chat"
      component={Chat}
      options={{
        tabBarLabel: 'Chat',
        tabBarIcon: ({color, focused}) => (
          <Icon
            name={focused ? 'chatbubbles' : 'chatbubbles-outline'}
            color={color}
            size={26}
          />
        ),
      }}
    />
    <Tabs.Screen
      name="Profile"
      component={Profile}
      options={{
        tabBarLabel: 'Profile',
        tabBarIcon: ({color, focused}) => (
          <Icon
            name={focused ? 'person' : 'person-outline'}
            color={color}
            size={26}
          />
        ),
      }}
    />
  </Tabs.Navigator>
);

const AppRouter = () => {
  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
        case 'ON_BOARDING':
          return {
            ...prevState,
            onboarding: action.onBoarding,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      onboarding: null,
      userToken: null,
    },
  );

  React.useEffect(() => {
    const bootstrapAsync = async () => {
      let userToken = null;
      let onBoarding = null;

      try {
        userToken = await AsyncStorage.getItem('userToken');
        onBoarding = await AsyncStorage.getItem('onboarding');
      } catch (e) {
        console.log(e);
      }

      console.log('userToken', userToken);
      console.log('onboarding', onBoarding);

      dispatch({type: 'RESTORE_TOKEN', token: userToken});
      dispatch({type: 'ON_BOARDING', onBoarding: onBoarding});
    };

    bootstrapAsync();
  }, []);

  const authContext = React.useMemo(
    () => ({
      signIn: async (data) => {
        try {
          await AsyncStorage.setItem('userToken', JSON.stringify(data));
        } catch (error) {}

        dispatch({type: 'SIGN_IN', token: data});
      },
      signOut: async () => {
        try {
          await AsyncStorage.removeItem('userToken');
        } catch (error) {}
        dispatch({type: 'SIGN_OUT'});
      },

      onBoarding: async () => {
        let onBoarding = null;
        try {
          onBoarding = 'onboard';
          await AsyncStorage.setItem('onboarding', onBoarding);
        } catch (error) {}
        dispatch({type: 'ON_BOARDING', onBoarding: onBoarding});
      },
    }),
    [],
  );

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <Stack.Navigator>
          {state.isLoading ? (
            <Stack.Screen
              name="Splash"
              component={Splash}
              options={{headerShown: false}}
            />
          ) : state.onboarding == null ? (
            <Stack.Screen
              name="Intro"
              component={Intro}
              options={{headerShown: false}}
            />
          ) : state.userToken == null ? (
            <Stack.Screen
              name="Auth"
              component={AuthScreen}
              options={{headerShown: false}}
            />
          ) : (
            <Stack.Screen
              name="Tabs"
              component={TabsScreen}
              options={{headerShown: false}}
            />
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
  );
};

export default AppRouter;
