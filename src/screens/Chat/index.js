import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {GiftedChat} from 'react-native-gifted-chat';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import storage from '@react-native-firebase/storage';

const Chat = () => {
  const [messages, setMessages] = useState([]);
  const [user, setUser] = useState({});
  const [avatar, setAvatar] = useState('');

  useEffect(() => {
    const user = auth().currentUser;
    console.log(user);
    if (user.photoURL == null) {
      storage()
        .ref('images/' + user.uid)
        .getDownloadURL()
        .then((url) => {
          setAvatar(url);
        })
        .catch((e) => console.log('getting downloadURL of image error => ', e));
    } else {
      setAvatar(user.photoURL);
    }
    setUser(user);
    getData();
    return () => {
      const db = database().ref('messages');
      if (db) {
        db.off();
      }
    };
  }, []);

  const getData = () => {
    database()
      .ref('messages')
      .limitToLast(20)
      .on('child_added', (snapshot) => {
        const value = snapshot.val();
        setMessages((prevMessage) => GiftedChat.append(prevMessage, value));
      });
  };

  const onSend = (messages = []) => {
    for (let i = 0; i < messages.length; i++) {
      database().ref('messages').push({
        _id: messages[i]._id,
        createdAt: database.ServerValue.TIMESTAMP,
        text: messages[i].text,
        user: messages[i].user,
      });
    }
  };

  return (
    <View style={{flex: 1}}>
      <GiftedChat
        messages={messages}
        onSend={(messages) => onSend(messages)}
        user={{
          _id: user ? user.uid : 1,
          avatar: avatar,
          name: user.displayName ? user.displayName : 'No Name',
        }}
      />
    </View>
  );
};

export default Chat;

const styles = StyleSheet.create({});
