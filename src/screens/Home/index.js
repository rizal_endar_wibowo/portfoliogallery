import React, {useState, useEffect} from 'react';
import {
  View,
  StatusBar,
  Text,
  Image,
  TouchableHighlight,
  ScrollView,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import ImageSlider from 'react-native-image-slider';
import Icon from 'react-native-vector-icons/AntDesign';
import Chart from '../../components/Chart';
import Menu from '../../components/Menu';
import styles from './styles';

const imagesMobile = [
  require('../../assets/images/mobile/1ac1d7c5cf336a610205644cf7eada5c.jpg'),
  require('../../assets/images/mobile/5497ef51abacbde38e4207d768d4a58d.jpg'),
  require('../../assets/images/mobile/a170a193867ec1d59a1c045cb6590cdf.jpg'),
  require('../../assets/images/mobile/b748d894e5f4ea2fd5a88545318385d1.jpg'),
  require('../../assets/images/mobile/d7463af0cd25ebcdede2dd6a380cc5e7.jpg'),
  require('../../assets/images/mobile/f0eaf51a0c50603a4d7c16d35aadd2c0.jpg'),
  require('../../assets/images/mobile/fc9a36b17fb8e7f1ea6ece31883bdf0f.jpg'),
];

const imagesWebsite = [
  require('../../assets/images/website/0cd8b09eb90e0607393defe65d2b426d.jpg'),
  require('../../assets/images/website/c65e645d617890f36eeaf7f0aa3c6f25.jpg'),
  require('../../assets/images/website/cfc0103cd086facb9b1f4fc7aea9685e.jpg'),
];

const imagesUiUx = [
  require('../../assets/images/uiux/71c7264e1c12836277562595d1b41aef.jpg'),
  require('../../assets/images/uiux/9697060e25d4c3d059c0eff224d59dfe.jpg'),
  require('../../assets/images/uiux/9a64ce17e97953db9faa5c00139bfd61.jpg'),
  require('../../assets/images/uiux/a9bd74ed9d32f1ed944665cc6e9102de.jpg'),
  require('../../assets/images/uiux/caf58a29ec8b717bea51703b4085974b.jpg'),
  require('../../assets/images/uiux/d135f7e74c5b9cb6396bc2b3337865f2.jpg'),
];

const Home = () => {
  const [menuActive, setMenuActive] = useState(0);

  useEffect(() => {
    console.log(menuActive);
  }, [menuActive]);

  return (
    <ScrollView>
      <View style={styles.container}>
        <StatusBar backgroundColor="#00569126" barStyle="dark-content" />

        <View style={styles.header}>
          <Text style={styles.headerText}>
            Welcome To {'\n'}Portfolio Gallery
          </Text>
          <View style={styles.searchBar}>
            <TextInput style={styles.search} placeholder="Search" />
            <TouchableOpacity>
              <Icon name="search1" size={20} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.body}>
          <View style={styles.chart}>
            <Text style={{fontSize: 18, fontWeight: 'bold', color: '#005691'}}>
              All Time Portfolio Posted
            </Text>
            <Chart />
          </View>
          <View style={styles.menu}>
            <Menu
              source={require('../../assets/images/mobile.png')}
              label="Mobile App"
              active={menuActive == 0 ? true : false}
              onPress={() => setMenuActive(0)}
            />
            <Menu
              source={require('../../assets/images/website.png')}
              label="Website"
              active={menuActive == 1 ? true : false}
              onPress={() => setMenuActive(1)}
            />
            <Menu
              source={require('../../assets/images/uiux.png')}
              label="Ui/UX"
              active={menuActive == 2 ? true : false}
              onPress={() => setMenuActive(2)}
            />
          </View>
          <TouchableOpacity style={styles.more}>
            <Text style={{fontSize: 18, fontWeight: 'bold', color: '#005691'}}>
              Portfolio Collection
            </Text>
            <Text style={{fontSize: 12, fontWeight: '200', color: '#005691'}}>
              More..
            </Text>
          </TouchableOpacity>
          <ImageSlider
            loopBothSides
            autoPlayWithInterval={3000}
            images={
              menuActive == 0
                ? imagesMobile
                : menuActive == 1
                ? imagesWebsite
                : imagesUiUx
            }
            customSlide={({index, item, style, width}) => (
              // It's important to put style here because it's got offset inside
              <View key={index} style={[style, styles.customSlide]}>
                <Image source={item} style={styles.customImage} />
              </View>
            )}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default Home;
