import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  headerText: {
    fontSize: 26,
    fontWeight: 'bold',
    color: '#005691',
    marginBottom: 20,
    textAlign: 'center',
  },
  card: {
    backgroundColor: 'white',
    width: '100%',
    borderRadius: 20,
    elevation: 3,
    padding: 20,
  },
  btnCircle: {
    width: 50,
    height: 50,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    elevation: 2,
    marginHorizontal: 10,
  },

  header: {
    backgroundColor: '#00569126',
    height: 250,
    width: '100%',
    borderBottomEndRadius: 30,
    borderBottomStartRadius: 30,
    justifyContent: 'center',
    padding: 10,
  },

  body: {flex: 1, paddingHorizontal: 10},

  menu: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: Dimensions.get('window').width,
    paddingHorizontal: 5,
    marginTop: -30,
  },
  menuItem: {
    flex: 1,
    marginHorizontal: 5,
    borderRadius: 10,
    padding: 10,
    backgroundColor: 'white',
    elevation: 2,
    alignItems: 'center',
  },

  imageItem: {
    width: 50,
    height: 50,
  },

  labelItem: {
    fontSize: 12,
    color: '#005691',
    marginTop: 5,
  },

  slider: {flex: 1, justifyContent: 'center'},

  contentText: {color: '#fff'},
  buttons: {
    zIndex: 1,
    height: 15,
    marginTop: -25,
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  button: {
    margin: 3,
    width: 15,
    height: 15,
    opacity: 0.9,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonSelected: {
    opacity: 1,
    color: '#005691',
  },
  customSlide: {
    flex: 1,
    backgroundColor: '#f2f2f2',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  customImage: {
    width: Dimensions.get('window').width - 30,
    height: 400,
  },
  searchBar: {
    backgroundColor: '#f2f2f2',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 30,
    paddingHorizontal: 20,
  },
  search: {
    flex: 1,
  },
  more: {
    flexDirection: 'row',
    marginTop: 20,
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingHorizontal: 10,
  },
  menuActive: {
    backgroundColor: '#004A7C',
  },
  chart: {
    height: 200,
    padding: 5,
    backgroundColor: 'white',
  },
});

export default styles;
