import React from 'react';
import {View, Text, Image, StatusBar} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider'; //import library atau module react-native-app-intro-slider
import Icon from 'react-native-vector-icons/Ionicons';
import {AuthContext} from '../../context';
import styles from './styles';

const slides = [
  {
    key: 1,
    title: 'Save Your Portfolio',
    text: 'Keep your portfolio neat and organized',
    image: require('../../assets/images/undraw_online_gallery.png'),
  },
  {
    key: 2,
    title: 'Share Your Porfolio',
    text: 'Share your best portfolio with the world',
    image: require('../../assets/images/undraw_portfolio.png'),
  },
  {
    key: 3,
    title: 'Easy to Access',
    text: 'Easily accessible to all users',
    image: require('../../assets/images/undraw_mobile_application.png'),
  },
];

const Intro = ({navigation}) => {
  const {onBoarding} = React.useContext(AuthContext);
  //menampilkan data slides kedalam renderItem
  const renderItem = ({item}) => {
    return (
      <View style={styles.slide}>
        <Text style={styles.title}>{item.title}</Text>
        <Image source={item.image} />
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
  };

  //fungsi ketika onboarding ada di list terakhir atau screen terakhir / ketika button done di klik
  const onDone = () => {
    onBoarding();
  };

  //mengcustom tampilan button done
  const renderDoneButton = () => {
    return (
      <View style={styles.btnCircle}>
        <Icon name="md-checkmark" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };

  //mengcustom tampilan next button
  const renderNextButton = () => {
    return (
      <View style={styles.btnCircle}>
        <Icon name="arrow-forward" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
      <View style={{flex: 1}}>
        {/* merender atau menjalankan library react-native-app-intro-slider */}
        <AppIntroSlider
          data={slides}
          onDone={onDone}
          renderItem={renderItem}
          renderDoneButton={renderDoneButton}
          renderNextButton={renderNextButton}
          keyExtractor={(item, index) => index.toString()}
          activeDotStyle={{backgroundColor: '#005691'}}
        />
      </View>
    </View>
  );
};

export default Intro;
