import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  btnCircle: {
    width: 50,
    height: 50,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#005691',
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F2F2F2',
  },

  container: {
    flex: 1,
  },

  title: {
    fontSize: 24,
    color: '#005691',
    fontWeight: 'bold',
  },
  text: {
    fontSize: 16,
    textAlign: 'center',
  },
});

export default styles;
