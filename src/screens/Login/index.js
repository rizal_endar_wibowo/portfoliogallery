import React, {useEffect, useState} from 'react';
import {View, Text, StatusBar, Image, ScrollView} from 'react-native';
import {TextInput, TouchableOpacity} from 'react-native-gesture-handler';
import WavyHeader from '../../components/WavyHeader';
import styles from './styles';
import Icon from 'react-native-vector-icons/Ionicons';
import Axios from 'axios';
import api from '../../api';
import AsyncStorage from '@react-native-community/async-storage';
import {GoogleSignin} from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth';
import TouchID from 'react-native-touch-id';
import {AuthContext} from '../../context';

const config = {
  title: 'Authentication Required',
  imageColor: '#005691',
  imageErrorColor: 'red',
  sensorDescription: 'Touch Sensor',
  sensorErrorDescription: 'Failed',
  cancelText: 'Cancel',
};

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const {signIn} = React.useContext(AuthContext);

  const onLoginPress = () => {
    return auth()
      .signInWithEmailAndPassword(email, password)
      .then((res) => {
        // navigation.replace('Tabs');
        alert('Login Berhasil');
        signIn(res.user);
      })
      .catch((err) => {
        console.log(err);
        alert('Login Gagal, Silahkan Coba Lagi!');
      });
  };

  // const onLoginPress = () => {
  //   let data = {
  //     email: email,
  //     password: password,
  //   };

  //   Axios.post(`${api}/login`, data, {timeout: 20000})
  //     .then((res) => {
  //       saveToken(res.data.token);
  //       navigation.replace('Tabs');
  //     })
  //     .catch((err) => {
  //       console.log('err', err);
  //       alert('Login Gagal, Silahkan Coba Lagi');
  //     });
  // };

  const saveToken = async (token) => {
    try {
      await AsyncStorage.setItem('token', token);
    } catch (error) {
      console.log('Save Error', error);
    }
  };

  const configGoogleSignIn = () => {
    GoogleSignin.configure({
      webClientId:
        '349938612476-msgji2l5i3gvn71k692bqqas5smle0nr.apps.googleusercontent.com',
      offlineAccess: false,
    });
  };

  useEffect(() => {
    configGoogleSignIn();
  }, []);

  const signInWithGoogle = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();

      const credential = auth.GoogleAuthProvider.credential(idToken);
      auth()
        .signInWithCredential(credential)
        .then((res) => {
          alert('Login Berhasil');
          signIn(res.user);
        })
        .catch((e) => {
          console.log(e);
          alert(e);
        });
    } catch (error) {
      console.log(error);
      alert(error);
    }
  };

  const signInWithFingerprint = () => {
    TouchID.authenticate('', config)
      .then((success) => {
        // navigation.replace('Tabs');
        signIn('auth');
      })
      .catch((error) => {
        alert('Authentication failed');
      });
  };

  return (
    <ScrollView>
      <View style={styles.container}>
        <StatusBar backgroundColor="#005691" barStyle="light-content" />
        <WavyHeader customStyles={styles.svgCurve} />
        <Image
          style={styles.logo}
          source={require('../../assets/images/logo_light.png')}
        />

        <View style={styles.card}>
          <Text style={styles.headerText}>Login</Text>
          <TextInput
            placeholder="Username/Email"
            style={styles.input}
            value={email}
            onChangeText={(email) => setEmail(email)}
          />
          <TextInput
            secureTextEntry
            placeholder="Password"
            style={styles.input}
            value={password}
            onChangeText={(password) => setPassword(password)}
          />
          <TouchableOpacity
            style={styles.button}
            onPress={() => onLoginPress()}>
            <Text style={styles.textButton}>LOGIN</Text>
          </TouchableOpacity>
        </View>

        <View style={{flexDirection: 'row', marginTop: 20}}>
          <TouchableOpacity
            style={styles.btnCircle}
            onPress={() => signInWithGoogle()}>
            <Icon name="logo-google" size={20} color="#005691" />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btnCircle}
            onPress={() => signInWithFingerprint()}>
            <Icon name="finger-print" size={20} color="#005691" />
          </TouchableOpacity>
        </View>

        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'flex-end',
            paddingBottom: 30,
            marginTop: 20,
          }}>
          <Text>Don't have an account?</Text>
          <TouchableOpacity onPress={() => navigation.navigate('Register')}>
            <Text style={{color: '#005691', marginStart: 10}}>Register</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default Login;
