import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 30,
    backgroundColor: '#F2F2F2',
  },
  svgCurve: {
    position: 'absolute',
    width: Dimensions.get('window').width,
  },
  headerText: {
    fontSize: 26,
    fontWeight: 'bold',
    color: '#005691',
    textAlign: 'center',
    marginBottom: 20,
  },

  logo: {
    width: 200,
    height: 70,
    marginVertical: 80,
  },
  card: {
    backgroundColor: 'white',
    width: '100%',
    borderRadius: 20,
    elevation: 3,
    padding: 10,
  },
  input: {
    borderColor: '#005691',
    borderWidth: 1,
    borderRadius: 30,
    marginBottom: 5,
    paddingHorizontal: 20,
    height: 45,
  },
  button: {
    backgroundColor: '#005691',
    height: 45,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 10,
  },
  textButton: {
    color: '#F2F2F2',
    fontSize: 16,
  },
  btnCircle: {
    width: 50,
    height: 50,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    elevation: 2,
    marginHorizontal: 10,
  },
});

export default styles;
