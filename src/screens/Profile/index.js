import React, {useEffect, useState} from 'react';
import {View, Text, StatusBar, Image} from 'react-native';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import {GoogleSignin} from '@react-native-community/google-signin';
import styles from './styles';
import Icon from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import AsyncStorage from '@react-native-community/async-storage';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import storage from '@react-native-firebase/storage';
import {AuthContext} from '../../context';

const Profile = ({navigation}) => {
  const [userInfo, setUserInfo] = useState(null);
  const [photo, setPhoto] = useState('');
  const [photoUrl, setPhotoUrl] = useState(null);

  const {signOut} = React.useContext(AuthContext);

  const configGoogleSignIn = () => {
    GoogleSignin.configure({
      webClientId:
        '349938612476-msgji2l5i3gvn71k692bqqas5smle0nr.apps.googleusercontent.com',
      offlineAccess: false,
    });
  };

  useEffect(() => {
    const getToken = async () => {
      try {
        const token = await AsyncStorage.getItem('token');
        return getVenue(token);
      } catch (error) {
        console.log(error);
      }
    };

    // getToken();
    configGoogleSignIn();
    getCurrentUser();
  }, [userInfo]);

  const getCurrentUser = async () => {
    if (await GoogleSignin.isSignedIn()) {
      const userInfo = await GoogleSignin.signInSilently();
      if (userInfo != null) {
        return setUserInfo({
          name: userInfo.user.name,
          photo: userInfo.user.photo,
          email: userInfo.user.email,
        });
      }
    }
    if (auth().currentUser) {
      const userInfo = auth().currentUser;

      storage()
        .ref('images/' + userInfo.uid)
        .getDownloadURL()
        .then((url) => {
          setPhotoUrl(url);
        })
        .catch((e) => console.log('getting downloadURL of image error => ', e));

      return setUserInfo({
        name: userInfo.displayName,
        photo: userInfo.photoURL == null ? photoUrl : userInfo.photoURL,
        email: userInfo.email,
      });
    }
  };

  const onLogoutPress = async () => {
    try {
      console.log(await GoogleSignin.isSignedIn());
      if (await GoogleSignin.isSignedIn()) {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      }
      await AsyncStorage.removeItem('userToken');
      console.log('logout');
      signOut();
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <ScrollView>
      <View style={styles.container}>
        <StatusBar backgroundColor="#005691" barStyle="light-content" />
        <View
          style={{
            width: '100%',
            alignItems: 'flex-end',
          }}>
          <TouchableOpacity
            onPress={() => onLogoutPress()}
            style={{
              width: 30,
              height: 30,
              margin: 20,
              borderRadius: 15,
              backgroundColor: '#f2f2f2',
              elevation: 2,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <AntDesign name="logout" size={20} color="#005691" />
          </TouchableOpacity>
        </View>

        <View style={styles.header}>
          <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
            <Image
              source={
                userInfo == null
                  ? require('../../assets/images/profile_pic.png')
                  : userInfo.photo != null
                  ? {uri: userInfo.photo}
                  : require('../../assets/images/profile_pic.png')
              }
              style={{height: 100, width: 100, borderRadius: 50}}
            />
            <TouchableOpacity
              style={{
                width: 30,
                height: 30,
                borderRadius: 15,
                backgroundColor: 'white',
                elevation: 2,
                justifyContent: 'center',
                alignItems: 'center',
                marginStart: -20,
              }}>
              <Icon name="camera-outline" size={20} color="#005691" />
            </TouchableOpacity>
          </View>
          <Text style={{color: 'white', fontSize: 18, fontWeight: '500'}}>
            {userInfo == null ? 'Nama Lengkap' : userInfo.name}
          </Text>
        </View>

        <View style={styles.body}>
          <View style={styles.card}>
            <TouchableOpacity
              style={{
                width: 30,
                height: 30,
                borderRadius: 15,
                backgroundColor: '#005691',
                elevation: 2,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'flex-end',
              }}>
              <AntDesign name="edit" size={20} color="#f2f2f2" />
            </TouchableOpacity>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                margin: 10,
              }}>
              <Text>Tanggal Lahir</Text>
              <Text>-</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                margin: 10,
              }}>
              <Text>Jenis Kelamin</Text>
              <Text>-</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                margin: 10,
              }}>
              <Text>Hobi</Text>
              <Text>-</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                margin: 10,
              }}>
              <Text>No. Telp</Text>
              <Text>08xxxxxxxxx</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                margin: 10,
              }}>
              <Text>Email</Text>
              <Text>
                {userInfo == null ? 'email@mail.com' : userInfo.email}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Profile;
