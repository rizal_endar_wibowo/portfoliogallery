import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#005691',
  },
  body: {
    backgroundColor: '#f2f2f2',
    flex: 1,
    width: '100%',
    padding: 30,
    borderTopEndRadius: 30,
    borderTopStartRadius: 30,
  },
  svgCurve: {
    position: 'absolute',
    width: Dimensions.get('window').width,
  },
  headerText: {
    fontSize: 26,
    fontWeight: 'bold',
    color: '#005691',
    textAlign: 'center',
    marginBottom: 20,
  },
  card: {
    backgroundColor: 'white',
    width: '100%',
    borderRadius: 20,
    elevation: 3,
    padding: 20,
  },
  btnCircle: {
    width: 50,
    height: 50,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    elevation: 2,
    marginHorizontal: 10,
  },

  header: {
    marginVertical: 20,
    paddingStart: 10,
    alignItems:'center'
  },
});

export default styles;
