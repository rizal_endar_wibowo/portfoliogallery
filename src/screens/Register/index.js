import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  Image,
  Modal,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import WavyHeader from '../../components/WavyHeader';
import styles from './styles';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/Ionicons';
import storage from '@react-native-firebase/storage';
import auth from '@react-native-firebase/auth';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';

const Register = ({navigation}) => {
  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState('back');
  const [photo, setPhoto] = useState(null);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [retypePassword, setRetypePassword] = useState('');
  const [name, setName] = useState('');

  const toggleCamera = () => {
    setType(type === 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    const option = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.takePictureAsync(option);
      setPhoto(data);
      setIsVisible(false);
    }
  };

  const uploadImage = (uri, filename) => {
    return storage()
      .ref(`images/${filename}`)
      .putFile(uri)
      .then((response) => {
        console.log(response);
      })
      .catch((error) => alert(error));
  };

  const registerAccount = () => {
    console.log('regis');
    if (password == '' || retypePassword == '' || name == '' || email == '') {
      return alert('Mohon isi seluruh data');
    }
    if (password != retypePassword) {
      return alert('Password tidak sesuai');
    }

    auth()
      .createUserWithEmailAndPassword(email, password)
      .then((res) => {
        res.user.updateProfile({
          displayName: name,
        });
        uploadImage(photo.uri, res.user.uid);
        alert('Register Berhasil');
      })
      .catch((err) => alert(err));
  };

  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{flex: 1}}
            type={type}
            ref={(ref) => {
              camera = ref;
            }}>
            <View
              style={{
                width: 50,
                height: 50,
                margin: 10,
                borderRadius: 35,
                backgroundColor: 'white',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <TouchableOpacity onPress={() => toggleCamera()}>
                <MaterialCommunity name="rotate-3d-variant" size={20} />
              </TouchableOpacity>
            </View>
            <View
              style={{
                height: '40%',
                width: 200,
                borderRadius: 100,
                borderColor: 'black',
                borderWidth: 2,
                alignSelf: 'center',
              }}
            />
            <View
              style={{
                height: '20%',
                width: 200,
                borderColor: 'black',
                borderWidth: 2,
                alignSelf: 'center',
                marginVertical: 20,
              }}
            />
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-end',
                alignItems: 'center',
                paddingBottom: 40,
              }}>
              <TouchableOpacity
                style={{
                  width: 70,
                  height: 70,
                  borderRadius: 35,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={() => takePicture()}>
                <MaterialCommunity name="camera" size={40} />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };

  return (
    <ScrollView>
      <View style={styles.container}>
        <StatusBar backgroundColor="#005691" barStyle="light-content" />
        <WavyHeader customStyles={styles.svgCurve} />
        {renderCamera()}
        <View style={styles.header}>
          <View style={{flexDirection: 'row', alignItems: 'flex-end'}}>
            <Image
              source={
                photo === null
                  ? require('../../assets/images/profile_pic.png')
                  : {uri: photo.uri}
              }
              style={{height: 100, width: 100, borderRadius: 50}}
            />
            <TouchableOpacity
              style={{
                width: 30,
                height: 30,
                borderRadius: 15,
                backgroundColor: 'white',
                elevation: 2,
                justifyContent: 'center',
                alignItems: 'center',
                marginStart: -20,
              }}
              onPress={() => setIsVisible(true)}>
              <Icon name="camera-outline" size={20} color="#005691" />
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.card}>
          <Text style={styles.headerText}>Register</Text>
          <TextInput
            placeholder="Nama Lengkap"
            style={styles.input}
            value={name}
            onChangeText={(name) => setName(name)}
          />
          <TextInput
            placeholder="Email"
            style={styles.input}
            value={email}
            onChangeText={(email) => setEmail(email)}
          />
          <TextInput
            secureTextEntry
            placeholder="Password"
            style={styles.input}
            value={password}
            onChangeText={(password) => setPassword(password)}
          />
          <TextInput
            secureTextEntry
            placeholder="Ulangi Password"
            style={styles.input}
            value={retypePassword}
            onChangeText={(retypePassword) => setRetypePassword(retypePassword)}
          />
          <TouchableOpacity
            style={styles.button}
            onPress={() => registerAccount()}>
            <Text style={styles.textButton}>REGISTER</Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'flex-end',
            marginTop: 20,
            paddingBottom: 30,
          }}>
          <Text>Already have an account?</Text>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Text style={{color: '#005691', marginStart: 10}}>Login</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default Register;
